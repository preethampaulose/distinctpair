package com.nyse;

import java.util.HashMap;
import java.util.Map;

public class DistinctPairs {

    private int[] list;
    private Map<Integer, Integer> distinctPairs = new HashMap<>();

    public DistinctPairs(int[] inputArray) {
	this.list = inputArray;
    }

    public Map<Integer, Integer> getDistinctPairsFor(int sum) {
	Map<Integer, Integer> trackingMap = new HashMap<>();
	if (this.list == null)
	    return new HashMap<>();
	for (Integer value : list) {
	    if (trackingMap.containsKey(value))
		addPairToMap(sum, trackingMap, value);
	    else if (!trackingMap.containsValue(value))
		trackingMap.put(sum - value, value);
	}
	printPairsToConsole();
	return this.distinctPairs;
    }

    private void printPairsToConsole() {
	this.distinctPairs.forEach((key, value) -> System.out.println("(" + key + "," + value + ")"));

    }

    private void addPairToMap(int sum, Map<Integer, Integer> trackingMap, Integer value) {
	if (trackingMap.get(value) != null)
	    this.distinctPairs.put(value, sum - value);
	// Mark the value used to avoid duplicate pairs
	trackingMap.put(sum - value, null);
    }

}
