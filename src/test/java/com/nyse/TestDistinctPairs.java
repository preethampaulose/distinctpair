package com.nyse;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class TestDistinctPairs {

    @Test
    public void emptyListReturnsAnEmptyPair() {
	int imputArray[] = {};

	Map<Integer, Integer> expectedPairs = new HashMap<>();

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(7);

	assertThat(pairs, is(expectedPairs));
    }

    @Test
    public void testContinuousNumbers() {
	int imputArray[] = { 1, 2, 3, 4, 5, 6 };

	Map<Integer, Integer> expectedPairs = new HashMap<>();
	expectedPairs.put(4, 3);
	expectedPairs.put(5, 2);
	expectedPairs.put(6, 1);

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(7);

	assertThat(pairs, is(expectedPairs));
    }

    @Test
    public void testRandomNumbersInAscendingOrder() {
	int imputArray[] = { 1, 3, 4, 6, 7, 8 };

	Map<Integer, Integer> expectedPairs = new HashMap<>();
	expectedPairs.put(4, 3);
	expectedPairs.put(6, 1);

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(7);

	assertThat(pairs, is(expectedPairs));
    }

    @Test
    public void testRandomNumbers() {
	int imputArray[] = { 1, 6, 4, 3, 2, 5 };

	Map<Integer, Integer> expectedPairs = new HashMap<>();
	expectedPairs.put(3, 4);
	expectedPairs.put(6, 1);
	expectedPairs.put(5, 2);

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(7);

	assertThat(pairs, is(expectedPairs));
    }

    @Test
    public void testRepeatedPairs() {
	int imputArray[] = { 1, 6, 1, 6, 1, 6 };

	Map<Integer, Integer> expectedPairs = new HashMap<>();
	expectedPairs.put(6, 1);

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(7);

	assertThat(pairs, is(expectedPairs));
    }

    @Test
    public void testSameNumberRepeated() {
	int imputArray[] = { 5, 5, 5, 5, 5, 5, 5, 5, 5 };

	Map<Integer, Integer> expectedPairs = new HashMap<>();
	expectedPairs.put(5, 5);

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(10);

	assertThat(pairs, is(expectedPairs));
    }

    @Test
    public void testForAllZeros() {
	int imputArray[] = { 0, 0, 0, 0, 0, 0, 0 };

	Map<Integer, Integer> expectedPairs = new HashMap<>();
	expectedPairs.put(0, 0);

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(0);

	assertThat(pairs, is(expectedPairs));
    }

    @Test
    public void testListWithNegativeNumbers() {
	int imputArray[] = { 1, 6, 5, -1, 8, -5, 12 };

	Map<Integer, Integer> expectedPairs = new HashMap<>();
	expectedPairs.put(6, 1);
	expectedPairs.put(8, -1);
	expectedPairs.put(12, -5);

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(7);

	assertThat(pairs, is(expectedPairs));
    }

    @Test
    public void testListWithLarrgeNumbers() {
	int imputArray[] = { 9999, -9992, 9999999, -9999992, -99999992, 99999999 };

	Map<Integer, Integer> expectedPairs = new HashMap<>();
	expectedPairs.put(-9992, 9999);
	expectedPairs.put(-9999992, 9999999);
	expectedPairs.put(99999999, -99999992);

	DistinctPairs dp = new DistinctPairs(imputArray);
	Map<Integer, Integer> pairs = dp.getDistinctPairsFor(7);

	assertThat(pairs, is(expectedPairs));
    }

}
